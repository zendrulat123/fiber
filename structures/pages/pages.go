package post

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/davecgh/go-spew/spew"
	"github.com/go-sql-driver/mysql"
	"github.com/gofiber/fiber/v2"
	conn "gitlab.com/zendrulat123/fiber/db"
)

type Post struct {
	ID     string `json:"id" form:"id" query:"id"`
	Userid string `json:"userid" form:"userid" query:"userid"`
	TITLE  string `json:"title" form:"title" query:"title"`
	URL    string `json:"url" form:"url" query:"url"`
	HTML   string `json:"html" form:"html" query:"html"`
	CSS    string `json:"css" form:"css" query:"css"`
	JS     string `json:"js" form:"js" query:"js"`
}

//CreateUser creates a Post
func (p Post) CreatePost(ui string, t string, u string, h string, c string, j string) bool {
	//opening database
	data := conn.Conn()
	// query
	stmt, err := data.Prepare("INSERT INTO post(userid, title, url, html, css, js) VALUES(?,?,?,?,?,?)")
	if err != nil {
		log.Fatal(err)
	}
	posttemp := Post{Userid: ui, TITLE: t, URL: u, HTML: h, CSS: c, JS: j}
	fmt.Println(posttemp, "- added to database")
	//add to the database
	res, err := stmt.Exec(posttemp.Userid, posttemp.TITLE, posttemp.URL, posttemp.HTML, posttemp.CSS, posttemp.JS)
	if err != nil {
		spew.Dump(err)
		if driverErr, ok := err.(*mysql.MySQLError); ok { // Now the error number is accessible directly
			if driverErr.Number == 1062 {
				// Handle the permission-denied error
				log.Println("Sorry, 1 already exists.")
				return false
			}
		}

	}
	//if error then print first and last id
	lastId, err := res.LastInsertId()
	if err != nil {
		spew.Dump(err)
		log.Fatal(err)
	}
	rowCnt, err := res.RowsAffected()
	if err != nil {
		spew.Dump(err)
		log.Fatal(err)
	}
	log.Printf("ID = %d, affected = %d\n", lastId, rowCnt)
	return true
}

//UpdatePost creates a Post
func UpdatePost(id string, ui string, t string, u string, h string, c string, j string) (error, bool) {
	//opening database
	data := conn.Conn()
	// query
	stmt, err := data.Prepare("UPDATE post SET userid=?, title=?, url=?, html=?, css=?, js=? where id=?")
	if err != nil {
		log.Fatal(err)
	}
	//initializing Post
	posttemp := Post{Userid: ui, TITLE: t, URL: u, HTML: h, CSS: c, JS: j, ID: id}
	//checking whats going into database because res below prints way too much info
	fmt.Println(posttemp, "- added to database")

	//add to the database
	res, err := stmt.Exec(posttemp.Userid, posttemp.TITLE, posttemp.URL, posttemp.HTML, posttemp.CSS, posttemp.JS, posttemp.ID)
	if err != nil {
		spew.Dump(err)
		if driverErr, ok := err.(*mysql.MySQLError); ok { // Now the error number is accessible directly
			if driverErr.Number == 1062 {
				// Handle the permission-denied error
				log.Println("Sorry, 1 already exists.")
				return err, false
			} else if driverErr.Number == 1064 {
				spew.Dump(err)
			} else {
				spew.Dump(err)

			}
		}
	}
	//if error then print first and last id
	lastId, err := res.LastInsertId()
	if err != nil {
		spew.Dump(err)
		log.Fatal(err)
	}
	rowCnt, err := res.RowsAffected()
	if err != nil {
		spew.Dump(err)
		log.Fatal(err)
	}
	log.Printf("ID = %d, affected = %d\n", lastId, rowCnt)
	return err, true
}

func Searchpost(url string, pi string) (error, bool) {
	//opening database
	var err error
	data := conn.Conn()
	if pi != "" {
		stmt, err := data.Prepare("SELECT url FROM post WHERE id=?")
		if err != nil {
			log.Fatal(err)
		}
		defer stmt.Close()
		err = stmt.QueryRow(pi).Scan(&url)
		switch err {
		case sql.ErrNoRows:
			fmt.Println("No rows were returned!")
			return err, false
		case nil:
			fmt.Println(pi, url)
			return err, true
		default:
			panic(err)
		}
	} else {
		fmt.Println("no page id was found")
		return err, false
	}
}
func SearchPageData(pageid string) (error, bool, Post) {
	//opening database
	var err error
	data := conn.Conn()
	p := Post{}
	if pageid != "" {
		stmt, err := data.Prepare("SELECT id, userid, title, url, html, css, js FROM post WHERE id=?")
		if err != nil {
			log.Fatal(err)
		}
		defer stmt.Close()
		p := Post{}
		err = stmt.QueryRow(pageid).Scan(&p.ID, &p.Userid, &p.TITLE, &p.URL, &p.HTML, &p.CSS, &p.JS)
		switch err {
		case sql.ErrNoRows:
			fmt.Println("No rows were returned!")
			return err, false, p
		case nil:
			spew.Dump(pageid, p)
			return err, true, p
		default:
			panic(err)
		}
	} else {
		fmt.Println("no page id was found")
		return err, false, p
	}
}

//DeletePage deletes a page
func DeletePage(c *fiber.Ctx) error {
	var err error
	p := new(Post)
	if err := c.BodyParser(p); err != nil {
		spew.Dump(err)
	}
	log.Println(p.ID, p.Userid, p.URL)
	data := conn.Conn()
	result, err := data.Exec("delete from post where id = ? and userid = ? and url = ?", p.ID, p.Userid, p.URL)
	if err != nil {
		spew.Dump(err)
		spew.Dump(result)
		return err
	} else {
		fmt.Println(result.RowsAffected())
		return err
	}
}
