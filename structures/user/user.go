package user

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/davecgh/go-spew/spew"
	conn "gitlab.com/zendrulat123/fiber/db"
)

type User struct {
	// ID       int    `json:"id" form:"id" query:"id"`
	// Email    string `json:"email" form:"email" query:"email"`
	// Password string `json:"password" form:"password" query:"password"`
	ID       int    `validate:"required,min=3,max=32"`
	Email    string `validate:"required,email,min=6,max=32"`
	Password string `validate:"required,min=3,max=32"`
}

//CreateUser creates a user
func (u User) CreateUser(e string, p string) bool {
	//opening database
	data := conn.Conn()
	// query
	stmt, err := data.Prepare("INSERT INTO user(email, password) VALUES(?, ?)")
	if err != nil {
		log.Fatal(err)
	}
	//initializing User
	userstemp := User{Email: e, Password: p}

	//add to the database
	res, err := stmt.Exec(userstemp.Email, userstemp.Password)
	if err != nil {
		log.Fatal(err)
	}
	//if error then print first and last id
	lastId, err := res.LastInsertId()
	if err != nil {
		log.Fatal(err)
	}
	rowCnt, err := res.RowsAffected()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("ID = %d, affected = %d\n", lastId, rowCnt)
	return true
}

func SearchUserData(Email string, Password string) (error, bool, User) {
	//opening database
	var err error
	data := conn.Conn()
	u := User{}
	if Email != "" {
		stmt, err := data.Prepare("SELECT id, email, password  FROM user WHERE email=? AND password=?")
		if err != nil {
			log.Fatal(err)
		}
		defer stmt.Close()
		u := User{}
		err = stmt.QueryRow(Email, Password).Scan(&u.ID, &u.Email, &u.Password)
		switch err {
		case sql.ErrNoRows:
			fmt.Println("No rows were returned!")
			return err, false, u
		case nil:
			spew.Dump(Email, u)
			return err, true, u
		default:
			panic(err)
		}
	} else {
		fmt.Println("no user email was found")
		return err, false, u
	}
}
