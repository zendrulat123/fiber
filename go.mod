module gitlab.com/zendrulat123/fiber

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gobuffalo/envy v1.9.0 // indirect
	github.com/gobuffalo/packd v1.0.0 // indirect
	github.com/gobuffalo/packr v1.30.1
	github.com/gofiber/fiber/v2 v2.2.3
	github.com/gofiber/template v1.6.4
	github.com/rogpeppe/go-internal v1.6.2 // indirect
	golang.org/x/sys v0.0.0-20201130171929-760e229fe7c5 // indirect
)
