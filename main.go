package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gobuffalo/packr"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/pprof"
	"github.com/gofiber/template/html"
	r "gitlab.com/zendrulat123/fiber/router"
)

func main() {

	engine := html.New("./views/templates", ".html")

	// Reload the templates on each render, good for development
	engine.Reload(true) // Optional. Default: false

	// Debug will print each template that is parsed, good for debugging
	engine.Debug(true) // Optional. Default: false

	// Layout defines the variable name that is used to yield templates within layouts
	engine.Layout("embed") // Optional. Default: "embed"

	app := fiber.New(fiber.Config{
		Views: engine,

		ErrorHandler: func(c *fiber.Ctx, err error) error {
			return c.Status(500).SendString(err.Error())
		},
	})

	// Or extend your config for customization
	// app.Use(basicauth.New(basicauth.Config{
	// 	Users: map[string]string{
	// 		"aa":    "aa",
	// 		"admin": "123456",
	// 	},
	// 	Realm: "Forbidden",
	// 	Authorizer: func(user, pass string) bool {

	// 		//regrab all users
	// 		DBallusers := allusers.GetAllUsers()
	// 		for _, thisuser := range DBallusers {
	// 			spew.Dump(thisuser)
	// 			//check if emails match
	// 			if thisuser.Email == user && thisuser.Password == pass {
	// 				//print stuff
	// 				fmt.Println("email match for user ", thisuser)
	// 				return true

	// 			}
	// 			spew.Dump(user, pass, "didnt match")

	// 		} //for

	// 		return false
	// 	},
	// 	Unauthorized: func(c *fiber.Ctx) error {

	// 		return c.SendFiale("./views/templates/not.html")
	// 	},
	// 	ContextUsername: "_user",
	// 	ContextPassword: "_pass",
	// }))
	// Provide a minimal config

	app.Use(pprof.New())

	// Default config
	app.Use(cors.New())

	// Or extend your config for customization
	app.Use(cors.New(cors.Config{
		AllowOrigins: "localhost",
		AllowHeaders: "Origin, Content-Type, Accept",
	}))

	//routes
	r.SetupRoutes(app)

	// Provide a minimal config
	// app.Use(filesystem.New(filesystem.Config{
	// 	Root: http.Dir("./assets/style"),
	// }))

	box := packr.NewBox("./views/templates")
	s, err := box.FindString("/index.html")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(s)
	//The outside folder has to be referenced and
	//the /views first part is the route url
	//the second ./views is the folder it gets from
	//when you reference the assets in the template file,
	//make sure you reference the route /views so it knows.
	//<img src="/views/me.png" alt=""> NO NEED FOR ./
	app.Static("/views", "./views")
	//app.Static("/", "./views/contentedit/1/assets/scripts/")
	// Provide a minimal config
	// app.Use(basicauth.New(basicauth.Config{
	// 	Users: map[string]string{
	// 		"john":  "doe",
	// 		"admin": "123456",
	// 	},
	// }))

	// Default middleware config
	app.Use(logger.New())

	// Or extend your config for customization
	app.Use(logger.New(logger.Config{
		Format:     "${pid} ${status} - ${method} ${path}\n",
		TimeFormat: "02-Jan-2006",
		TimeZone:   "America/New_York",
		Output:     os.Stdout,
	}))
	app.Listen(":3000")
}
