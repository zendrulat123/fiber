package router

import (
	"github.com/gofiber/fiber/v2"
	d "gitlab.com/zendrulat123/fiber/handle/processing/dashboardprocessing"
	e "gitlab.com/zendrulat123/fiber/handle/processing/editorprocess"
	c "gitlab.com/zendrulat123/fiber/handle/serving/contentedit"
	dd "gitlab.com/zendrulat123/fiber/handle/serving/dashboard"
	dp "gitlab.com/zendrulat123/fiber/handle/serving/dashpages"
	l "gitlab.com/zendrulat123/fiber/handle/serving/login"
	np "gitlab.com/zendrulat123/fiber/handle/serving/newpage"
	p "gitlab.com/zendrulat123/fiber/handle/serving/pageview"
	pa "gitlab.com/zendrulat123/fiber/handle/servingclient/page"

	dpage "gitlab.com/zendrulat123/fiber/structures/pages"
)

func SetupRoutes(app *fiber.App) {

	//serving content
	app.Get("/contentedit/:userid/:pageid?/:url?/:title?", c.Contentedit) //editor //login form
	app.Get("/dashboard-home/:userid", dd.Dashboard)                      //dashboard after login
	app.Get("/dashboardpages/:userid/", dp.Dashpages)                     //dashboard pages
	app.Get("/edit/:userid/:pageid/:url/:title", p.Pager)
	app.Get("/newpage/:userid", np.Newpage) //process content editor
	app.Get("/", l.Login)

	//show the page for clients
	app.Get("/:url", pa.ShowPage) //actual page view

	//services
	app.Post("/dashboardprocessing", d.Dashboardprocessing)
	app.Post("/editor/:userid/:pageid?/:url?", e.Editorprocess) //process content editor
	app.Post("/delete/:userid/:pageid/:url", dpage.DeletePage)
}
