package post

import (
	"fmt"

	conn "gitlab.com/zendrulat123/fiber/db"
)

type Post struct {
	ID     string `json:"id" form:"id" query:"id"`
	Userid string `json:"userid" form:"userid" query:"userid"`
	TITLE  string `json:"title" form:"title" query:"title"`
	URL    string `json:"url" form:"url" query:"url"`
	HTML   string `json:"html" form:"html" query:"html"`
	CSS    string `json:"css" form:"css" query:"css"`
	JS     string `json:"js" form:"js" query:"js"`
}

//CreateUser creates a Post
func GetAllDataPosts() []Post {
	//opening database
	data := conn.Conn()

	var (
		id     string
		userid string
		title  string
		url    string
		html   string
		css    string
		js     string
		post   []Post
	)
	i := 0
	//get from database
	rows, err := data.Query("select id, userid, title, url, html, css, js from post")
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		err := rows.Scan(&id, &userid, &title, &url, &html, &css, &js)
		if err != nil {
			fmt.Println(err)
		} else {
			i++
			fmt.Println("scan ", i)
		}
		p := Post{ID: id, Userid: userid, TITLE: title, URL: url, HTML: html, CSS: css, JS: js}
		post = append(post, p)

	}
	defer rows.Close()
	defer data.Close()
	return post
}
