package dashboardprocessing

import (
	"fmt"
	"regexp"

	"github.com/davecgh/go-spew/spew"
	"github.com/gofiber/fiber/v2"

	users "gitlab.com/zendrulat123/fiber/structures/user"
)

//Dashboardprocessing check if user is there
func Dashboardprocessing(c *fiber.Ctx) error {
	var err error
	var email string
	var pass string
	u := users.User{}
	//check data
	if bools, err := regexp.MatchString(`^([a-z]{2,4})$`, c.FormValue("email")); !bools {
		fmt.Println(err, bools)
	} else {
		email = c.FormValue("email")
	}
	if err != nil {
		spew.Dump(err)
	}
	if bools, err := regexp.MatchString(`^([a-z]{2,4})$`, c.FormValue("password")); !bools {
		fmt.Println(err, bools)
	} else {
		pass = c.FormValue("password")
	}
	if err != nil {
		spew.Dump(err)
	}
	//!actual logic of checking user
	err, check, u := users.SearchUserData(email, pass)
	if check != false {
		//add userid to the url
		v := fmt.Sprint("dashboard-home/", u.ID)
		c.Redirect(v)
		return c.Render("dashboard", fiber.Map{"User": u}) //Render
	} else { //didnt find user
		fmt.Println("email didnt match, so creating user")
		//add data to database
		check := users.User.CreateUser(u, u.Email, u.Password)
		if check != true {
			fmt.Println("user not created")
		}
		return c.Render("index", fiber.Map{"User": u}) //Render
	}

}
