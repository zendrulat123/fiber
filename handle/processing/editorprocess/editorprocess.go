package editor

import (
	"fmt"
	"strings"

	"github.com/davecgh/go-spew/spew"
	"github.com/gofiber/fiber/v2"
	p "gitlab.com/zendrulat123/fiber/structures/pages"
)

//"/editor/:userid/:pageid?/:url?"
func Editorprocess(c *fiber.Ctx) error {
	//URL DATA
	pageid := c.Params("pageid")
	userids := c.Params("userid")
	formUrls := c.FormValue("url")
	trimurls := strings.Trim(formUrls, "/")
	Title := c.FormValue("title")
	html := c.FormValue("html")
	css := c.FormValue("css")
	js := c.FormValue("js")
	v := fmt.Sprint("/dashboardpages/", userids)

	err, check := p.Searchpost(trimurls, pageid)
	if err != nil {
		spew.Dump(err)
	}
	postcode := p.Post{Userid: userids, TITLE: Title, URL: trimurls, HTML: html, CSS: css, JS: js}
	if check != false {
		fmt.Println("url already in database")
		err, check := p.UpdatePost(pageid, userids, Title, trimurls, html, css, js)
		if check != false {
			fmt.Println("url already in database")
		}
		if err != nil {
			spew.Dump(err)
		}
	} else {
		//SAVE IN DATABASE
		check := p.Post.CreatePost(postcode, postcode.Userid, postcode.TITLE, postcode.URL, postcode.HTML, postcode.CSS, postcode.JS)
		if check != true {
			fmt.Println("post not created")
		}
	}
	c.Redirect(v)
	return c.Render("dashboard", fiber.Map{
		"Page": postcode,
	})

}
