package contentedit

import (
	"strings"

	"github.com/davecgh/go-spew/spew"
	"github.com/gofiber/fiber/v2"
	p "gitlab.com/zendrulat123/fiber/structures/pages"
)

//Contentedit serves contentedit page
func Contentedit(c *fiber.Ctx) error {
	url := c.Params("url")
	userid := c.Params("userid")
	pageid := c.Params("pageid")
	title := c.Params("title")

	trimurls := strings.Trim(url, "/")
	if trimurls != "" {
		err, check, Posts := p.SearchPageData(pageid)
		if err != nil {
			spew.Dump(err)
		}
		if check != false {

			return c.Render("contentedit", fiber.Map{
				"Pageid": pageid,
				"URL":    url,
				"User":   userid,
				"Title":  title,
				"HTML":   Posts.HTML,
				"JS":     Posts.JS,
				"CSS":    Posts.CSS,
			})
		}
	}
	return c.Render("dashpages", fiber.Map{
		"Pageid": pageid,
		"User":   userid,
		"URL":    url,
	}) //Render
}
