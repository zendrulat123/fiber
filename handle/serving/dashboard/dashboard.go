package dashbord

import (
	"github.com/gofiber/fiber/v2"
)

func Dashboard(c *fiber.Ctx) error {
	userId := c.Params("userid")

	return c.Render("dashboard", fiber.Map{
		"User": userId,
	})
}
