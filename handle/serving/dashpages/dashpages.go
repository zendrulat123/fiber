package dashpages

import (
	"fmt"
	"strings"

	"github.com/gofiber/fiber/v2"

	allpages "gitlab.com/zendrulat123/fiber/db/getallpages"
)

func Dashpages(c *fiber.Ctx) error {
	userId := c.Params("userid")

	allpage := allpages.GetAllPosts()
	type Pagedata struct {
		ID     []string
		Title  []string
		URLS   []string
		userid string
	}
	var pagedata Pagedata
	var i int
	for _, thisuserpage := range allpage {
		thistrimurl := strings.Trim(thisuserpage.URL, "/")
		if userId == thisuserpage.Userid {
			i++
			fmt.Println("this is pages", pagedata, i)
			pagedata.URLS = append(pagedata.URLS, thistrimurl)
			pagedata.Title = append(pagedata.Title, thisuserpage.TITLE)
			pagedata.ID = append(pagedata.ID, thisuserpage.ID)

		} else {
			fmt.Println(" ids did not match", userId, thisuserpage.Userid)
		}
	}
	return c.Render("dashpages", fiber.Map{
		"ID":     pagedata.ID,
		"TITLE":  pagedata.Title,
		"URL":    pagedata.URLS,
		"Userid": userId}) //Render
}
