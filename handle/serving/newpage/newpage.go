package newpage

import (
	"github.com/gofiber/fiber/v2"
)

//Contentedit serves contentedit page
func Newpage(c *fiber.Ctx) error {

	userid := c.Params("userid")

	return c.Render("newpage", fiber.Map{

		"User": userid,
	})
}
