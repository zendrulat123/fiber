package page

import (
	"strings"

	"github.com/davecgh/go-spew/spew"
	"github.com/gofiber/fiber/v2"
	p "gitlab.com/zendrulat123/fiber/structures/pages"
)

func Pager(c *fiber.Ctx) error {
	//grab data
	userID := c.Params("userid")
	URL := c.Params("url")
	pageid := c.Params("pageid")
	trimurls := strings.Trim(URL, "/")
	if trimurls != "" {
		err, check, Posts := p.SearchPageData(pageid)
		if err != nil {
			spew.Dump(err)
		}
		if check != false {
			return c.Render("pages", fiber.Map{
				"Pageid": pageid,
				"Title":  Posts.TITLE,
				"User":   userID,
				"URL":    URL,
				"HTML":   Posts.HTML,
				"JS":     Posts.JS,
				"CSS":    Posts.CSS,
			}) //Render
		}
	}
	return c.Render("dashpages", fiber.Map{
		"Pageid": pageid,
		"User":   userID,
		"URL":    URL,
	}) //Render
}
