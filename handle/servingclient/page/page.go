package page

import (
	"fmt"
	"strings"

	"github.com/gofiber/fiber/v2"
	allpagedata "gitlab.com/zendrulat123/fiber/db/allpagedata"
)

func ShowPage(c *fiber.Ctx) error {
	//grab data

	Url := c.Params("url")
	allpages := allpagedata.GetAllDataPosts()

	for _, thispage := range allpages {
		//parse this url to a string
		t := strings.Replace(thispage.URL, "/", "", -1)
		//if url are found then send template/data
		if Url == t {

			return c.Render("clientpage", fiber.Map{
				"Title": thispage.TITLE,
				"URL":   Url,
				"HTML":  thispage.HTML,
				"JS":    thispage.JS,
				"CSS":   thispage.CSS,
			}) //Render

		} else {
			fmt.Println(Url, "!=", t)
		}
	}
	return c.Render("dashpages", fiber.Map{}) //Render
}
